package com.silvercoding.locit.ui.fragments.home

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.PermissionChecker
import androidx.core.content.PermissionChecker.checkSelfPermission
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import com.silvercoding.locit.MainActivity
import com.silvercoding.locit.R
import com.silvercoding.locit.helpers.isAboveMarsh
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*

class HomeFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
    GoogleMap.OnMarkerDragListener {

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 100
        private const val LOCATION_SETTINGS_REQUEST_CODE = 200
    }

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var mMap: GoogleMap
    private lateinit var geocoder: Geocoder
    private lateinit var mContext: Context
    private var location: Location? = null
    private lateinit var marker: Marker

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mContext = activity as MainActivity
        geocoder = Geocoder(mContext, Locale.getDefault())
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val rootView = inflater.inflate(R.layout.fragment_home, container, false)
        val mapFragment = this.childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requestLocation()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            LOCATION_SETTINGS_REQUEST_CODE -> {
                when (resultCode) {
                    AppCompatActivity.RESULT_OK -> {
                        // User enabled Location Settings
                        applyLocationRequest()
                    }
                    AppCompatActivity.RESULT_CANCELED -> {

                    }
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun requestLocation() {
        isAboveMarsh(code = {
            if (checkSelfPermission(
                    mContext,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PermissionChecker.PERMISSION_GRANTED
            ) {
                // Permission already granted
                showLocationSettingsPopup()
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION))
                    Toast.makeText(
                        mContext,
                        "Location permission needed to access your device location",
                        Toast.LENGTH_SHORT
                    ).show()
                // Runtime request location permission
                requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        }, other = {
            // We don't need location permissions
            showLocationSettingsPopup()
        })
    }

    @SuppressLint("MissingPermission")
    private fun applyLocationRequest() {
        LocationServices.getFusedLocationProviderClient(mContext)
            .requestLocationUpdates(LocationRequest().apply {
                interval = 1000
                fastestInterval = 1000
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            }, object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult) {
                    super.onLocationResult(locationResult)
                    removeLocationRequest(this)
                    homeViewModel.currentLocation.value = locationResult.locations.firstOrNull()
                    locationResult.locations.firstOrNull()?.let {
                        reverseGeocode(LatLng(it.latitude, it.longitude))
                    } ?: run {
                        reverseGeocode(null)
                    }
                }
            }, Looper.getMainLooper())
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap?) {
        if (googleMap == null) return
        mMap = googleMap
        mMap.apply {
            mapType = GoogleMap.MAP_TYPE_HYBRID
            uiSettings.isCompassEnabled = true
            uiSettings.isMyLocationButtonEnabled = true
            isMyLocationEnabled = true
        }

        homeViewModel.currentLocation.observe(viewLifecycleOwner, {
            it?.let {
                val latLng = LatLng(it.latitude, it.longitude)
                marker = mMap.addMarker(
                    MarkerOptions()
                        .draggable(true)
                        .position(latLng)
                        .title("Long press to drag")
                )
                marker.showInfoWindow()

                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17f))
                mMap.setOnMarkerClickListener(this@HomeFragment)

            }
        })

        homeViewModel.currentAddress.observe(viewLifecycleOwner, {
            it?.let {
                geocodeResult.setText(it)
            }
        })
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        return false
    }

    private fun showLocationSettingsPopup() {
        val locationSettingsResponse: Task<LocationSettingsResponse>
        val locationSettingsRequestBuilder = LocationSettingsRequest.Builder().apply {
            addLocationRequest(LocationRequest.create().apply {
                this.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            })
        }
        locationSettingsRequestBuilder.also {
            locationSettingsResponse =
                LocationServices.getSettingsClient(requireContext())
                    .checkLocationSettings(it.build())

            locationSettingsResponse.addOnCompleteListener { task ->
                try {
                    task.getResult(ApiException::class.java)
                    applyLocationRequest()
                } catch (apiEx: ApiException) {
                    when (apiEx.statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                            try {
                                (apiEx as ResolvableApiException).startResolutionForResult(
                                    activity,
                                    LOCATION_SETTINGS_REQUEST_CODE
                                )
                            } catch (e: IntentSender.SendIntentException) {
                                Toast.makeText(mContext, "${e.message}", Toast.LENGTH_SHORT)
                                    .show()
                            } catch (e: ClassCastException) {
                                Toast.makeText(mContext, "${e.message}", Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                            showSettingsAlert()
                        }
                    }
                }
            }
        }
    }

    private fun showSettingsAlert() {
        AlertDialog.Builder(requireContext()).apply {
            setTitle("Allow locit to access your location?")
            setMessage("We need access to your location to show you relevant search results")
            setPositiveButton("Allow") { _, _ ->
                Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS).apply {
                    // searchMode(false)
                    startActivity(this)
                }
            }
            setNegativeButton("Cancel") { dialog, _ ->
                // searchMode(false)
                dialog.cancel()
            }
        }.also {
            it.show()
        }
    }

    private fun removeLocationRequest(locationCallback: LocationCallback) {
        LocationServices.getFusedLocationProviderClient(mContext)
            .removeLocationUpdates(locationCallback)
    }

    private fun reverseGeocode(latLng: LatLng?) {
        latLng?.let {
            val results = try {
                geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
            } catch (e: Exception) {
                null
            }

            results?.let { addresses ->
                homeViewModel.currentAddress.value = addresses.first().getAddressLine(0)
            } ?: run {
                homeViewModel.currentAddress.value = "Could not resolve address"
            }
        } ?: run {
            homeViewModel.currentAddress.value = "Could not resolve address"
        }
    }

    override fun onMarkerDragStart(p0: Marker?) {
        p0?.title = "Move to a place that you want"
    }

    override fun onMarkerDrag(p0: Marker?) {}

    override fun onMarkerDragEnd(mk: Marker?) {
        mk?.let {
            it.title = "Long press to drag"
            reverseGeocode(LatLng(it.position.latitude, it.position.longitude))
        }
    }
}