package com.silvercoding.locit.ui.fragments.home

import android.location.Location
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {

    var currentLocation = MutableLiveData<Location>()
    var currentAddress = MutableLiveData<String>()
}